{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE BangPatterns #-}
module Poisson
    ( poissonSample
    ) where

import Linear.V2 
import Linear.Vector
import Linear.Affine
import Control.Monad.Random.Lazy
import Control.Lens
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import Data.Maybe (catMaybes)

data Bridson = Bridson {
    _kFactor :: !Int,
    _samples :: !(Map (V2 Int) (Bool, V2 Float)),
    _bridsonSize :: !(V2 Float),
    _bridsonRadius :: !Float
  } deriving Show

makeLenses ''Bridson

isActive :: Bridson -> Bool
isActive = orOf $ samples.traverse._1

cellSize :: Float -> Float
cellSize r = r/(sqrt 2)

cellFor :: Float -> V2 Float -> V2 Int
cellFor r = fmap $ floor . (/(cellSize r))

setCell :: Bridson -> (Bool, V2 Float) -> Bridson
setCell b (a, p) = samples.(at (cellFor (b^.bridsonRadius) p)) ?~ (a, p) $ b

addPoint :: Bridson -> V2 Float -> Bridson
addPoint b p = setCell b (True, p)

deactivatePoint :: Bridson -> V2 Float -> Bridson
deactivatePoint b p = setCell b (False, p)

newBridson :: Int -> V2 Float -> Float -> Bridson
newBridson k sz r = Bridson k Map.empty sz r

randomElement :: RandomGen g => [a] -> Rand g (Maybe a)
randomElement [] = pure Nothing
randomElement xs = ((xs ^?).ix) <$> liftRand (randomR (0, (length xs) -1))

selectActivePoint :: RandomGen g => Bridson -> Rand g (Maybe (V2 Float))
selectActivePoint b = randomElement $ b^..samples.traverse.(filtered fst)._2

randomVector :: (Traversable v, Additive v, RandomGen g) => Rand g (v Float)
randomVector = traverse (const randomFloat) zero
  where
    randomFloat = liftRand $ randomR (0::Float, 1::Float) 

randomUnit :: RandomGen g => Rand g (V2 Float)
randomUnit = angle <$> (liftRand $ randomR (0, pi*2))

randomRing :: RandomGen g => Float -> Rand g (V2 Float)
randomRing r = do
    unit <- randomUnit
    rad <- liftRand $ randomR (r, r*2)
    return $ rad *^ unit

candidateNeighbour :: RandomGen g => Float -> V2 Float -> Rand g (V2 Float)
candidateNeighbour r c = (c ^+^) <$> randomRing r

withinAABB :: V2 Float -> V2 Float -> V2 Float -> Bool
withinAABB min max v = (v^._x > min^._x) && (v^._y > min^._y) && (v^._x < max^._x) && (v^._y < max^._y)

candidateIsValid :: Bridson -> V2 Float -> Bool
candidateIsValid b v = (withinAABB (V2 0 0) (b^.bridsonSize) v) && allNeighboursOk
  where
    cell = cellFor (b^.bridsonRadius) v
    neighbouringCells = [cell ^+^ (V2 x y) | x <- [-2..2], y <- [-2..2]]
    allNeighbours = catMaybes $ (fmap snd).((flip Map.lookup) (b^.samples)) <$> neighbouringCells
    neighbourOk = (> (b^.bridsonRadius)).(distanceA v)
    allNeighboursOk =  all neighbourOk allNeighbours 

poissonStep ::RandomGen g =>  Bridson -> Rand g (Maybe Bridson)
poissonStep b = do
    point <- selectActivePoint b
    case point of
        Nothing -> return Nothing
        Just p -> do
            candidates <- replicateM (b^.kFactor) (candidateNeighbour (b^.bridsonRadius) p)
            let validCandidates = filter (candidateIsValid b) candidates
            case validCandidates of
                [] -> return $ Just $ deactivatePoint b p
                newPoint:_ -> return $ Just $ addPoint b newPoint

untilFailure :: Monad m => (a -> m (Maybe a)) -> a -> m a
untilFailure f a = do
    r <- f a
    case r of
        Nothing -> return a
        Just next -> untilFailure f next

poissonSample :: RandomGen g => V2 Float -> Float -> Int -> Rand g [V2 Float]
poissonSample range radius k = (^..samples.traverse._2) <$> (untilFailure poissonStep initial)
  where 
    initial = addPoint (newBridson k range radius) (0.5 *^ range)
