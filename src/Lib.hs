{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE BangPatterns #-}
module Lib
    ( someFunc
    ) where

import Linear.V2 
import Linear.Vector
import Linear.Metric
import Control.Lens
import Poisson (poissonSample)
import Svg
import Delaunay
import Edge
import Control.Monad.Random.Lazy

edgeFun :: V2 Float -> V2 Float -> Bool
edgeFun sz x = n < 0.3 || (n > 0.4 && n < 0.45)
  where
    r = norm (x ^-^ (sz ^* 0.5)) 
    n = (r * sqrt 2) / (norm sz)


someFunc :: IO ()
someFunc = do
  let size = V2 800 800
  let r = 20
  sample <- evalRandIO $ poissonSample size r 30
  let delaunay = triangulate sample
  let voronoi = makeVoronoi delaunay
  --let lines = delaunayHalfedges delaunay
  let lines = extractEdges (edgeFun size) voronoi
  putStrLn $ pointsAndLinesAsSvg size (r/2) sample lines
