module Stl 
( toSTL
) where
import Linear.V3
import Linear.Affine
import Linear.Vector
import Linear.Metric (normalize)
import Data.List
import qualified Data.Text as T

type Tri = (V3 Float, V3 Float, V3 Float)

decompose :: Float -> (Float,Int)
decompose val = if mant2 > 0 
                     then (mant10,ex10)
                     else (-mant10,ex10)
  where
        (mant2,ex2) = decodeFloat val
        res = logBase 10 (fromIntegral (abs mant2)::Float) + logBase 10 (2 ** (fromIntegral ex2::Float)) 
        ex10 = floor res
        mant10 = 10**(res - (fromIntegral ex10::Float))

ingen :: Float -> (Float,Int)
ingen val 
  | mod ex 3 == 0 = (mant,ex)
  | mod ex 3 == 1 = (mant*10,ex-1)
  | mod ex 3 == 2 = (mant*100,ex-2)
  where
    (mant,ex) = decompose val

showEng :: Float -> String
showEng a = (show mant) ++ "e" ++ (show ex)
  where 
    (mant, ex) = decompose a

vertexString :: V3 Float -> T.Text
vertexString (V3 a b c) = T.pack ("        vertex " ++ (showEng a) ++ " " ++ (showEng b) ++ " " ++ (showEng c) ++ "\n")

triangleText :: Tri -> T.Text
triangleText (a, b, c) = T.concat [normalText, ol, as, bs, cs, el, end]
    where
        V3 nx ny nz = normalize $ (b ^-^ a) `cross` (c ^-^ a)
        normalText = T.pack("facet normal " ++ (showEng nx) ++ " "++ (showEng ny) ++ " " ++ (showEng nz) ++ "\n")
        ol = T.pack "    outer loop\n"
        as = vertexString a
        bs = vertexString b
        cs = vertexString c
        el = T.pack "    endloop\n"
        end = T.pack "endfacet\n"


toSTL :: [Tri] -> T.Text
toSTL tris = T.concat [start, body, end]
  where 
    start = T.pack "solid csg-result\n"
    body = T.concat $ map triangleText tris
    end = T.pack "endsolid csg-result\n"
