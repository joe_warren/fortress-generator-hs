{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE BangPatterns #-}
module Delaunay 
( Delaunay
, delaunayHalfedges
, Voronoi
, makeVoronoi
, voronoiHalfedges
, triangulate
) where

-- this is based on 
--https://github.com/mourner/delaunator-rs/blob/master/src/lib.rs

import Linear.V2 
import Linear.Vector hiding (Vector)
import qualified Data.Vector as Vec
import Data.Vector (Vector, (!), (!?))
import Linear.Affine hiding (Vector)
import Linear.Metric hiding (Vector)
import Linear.Epsilon (nearZero)
import Control.Monad.Random.Lazy
import Control.Lens
import qualified Data.Foldable as Foldable
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import Data.Maybe (catMaybes)
import Data.List (sortBy, foldl')
import Data.Function (on)
import System.IO.Unsafe (unsafePerformIO)
import Control.Monad.State
import qualified Algebra.Graph.Labelled.AdjacencyMap as GR
import Algebra.Graph.Labelled.AdjacencyMap ((-<), (>-))
import Data.Monoid (First (..), getFirst)

-- Represents the area outside of the triangulation.
-- Halfedges on the convex hull (which don't have an adjacent halfedge)
-- will have this value.
empty :: Int
empty = -1

data Delaunay = Delaunay {
    _points :: Vector (V2 Float), 
    _triangles :: Vector Int, 
    _halfedges :: Vector Int, 
    _hull :: Vector Int
}

makeLenses ''Delaunay

newDelaunay :: Vector (V2 Float) -> Delaunay
newDelaunay points = Delaunay points Vec.empty Vec.empty Vec.empty

addTriangle' :: (Int,Int, Int) -> (Int, Int, Int) -> State Delaunay Int
addTriangle' (i1, i2, i3) (a, b, c) = do
    t <- (Vec.length . (^.triangles)) <$> get
    modify (triangles %~ (Vec.++ (Vec.fromList [i1, i2, i3]))) 
    modify (halfedges %~ (Vec.++ (Vec.fromList [a, b, c])))
    modify (halfedges %~ (if a /= empty then (ix a) .~ t  else id))
    modify (halfedges %~ (if b /= empty then (ix b) .~ t  else id))
    modify (halfedges %~ (if c /= empty then (ix c) .~ t  else id)) 
    return t

addTriangle :: (Int,Int, Int) -> (Int, Int, Int) -> State (Hull, Delaunay) Int
addTriangle i a = zoom _2 (addTriangle' i a)

sides :: [a] -> [(a, a)]
sides (x1:x2:xs) = (x1, x2) : (sides xs)
sides _ = [] 

trios :: [a] -> [(a, a, a)]
trios (x1:x2:x3:xs) = (x1, x2, x3) : (trios xs)
trios _ = [] 

delaunayHalfedges :: Delaunay -> [(V2 Float, V2 Float)]
delaunayHalfedges d = do
    (a, b, c) <- trios $  Vec.toList $ (((d^.points) !) <$> (d^.triangles))
    [(a, b), (b, c), (c, a)]

data Hull = Hull {
    _hullPrev :: Vector Int, -- edge to prev edge
    _hullNext :: Vector Int, -- edge to next edge
    _hullTri :: Vector Int,  -- edge to adjacent triangle
    _hullHash :: Vector Int, -- angular edge hash
    _start :: Int, 
    _center :: V2 Float
} deriving Show

makeLenses ''Hull

hashKey :: Hull -> V2 Float -> Int
hashKey h v = (floor ((fromIntegral len) * a)) `mod` len
  where 
    d = v ^-^ (h^.center) 
    p = (d^._x) / (Foldable.sum $ abs <$> d)
    a = (if d^._y > 0 then 3 - p else 1 + p) / 4 :: Float
    len = Vec.length (h ^. hullHash)

hashEdge :: V2 Float -> Int -> Hull -> Hull
hashEdge p i h = h & hullHash . (ix (hashKey h p)) .~ i

newHull :: Int -> V2 Float -> Int -> Int -> Int -> Vector (V2 Float) -> Hull
newHull n center i0 i1 i2 points = let hashLen = floor . sqrt . fromIntegral $ n in
    Hull (Vec.replicate n 0) (Vec.replicate n 0) (Vec.replicate n 0) (Vec.replicate hashLen empty) i0 center &
        hullNext . (ix i0) .~ i1 & 
        hullPrev . (ix i2) .~ i1 & 
        hullNext . (ix i1) .~ i2 & 
        hullPrev . (ix i0) .~ i2 & 
        hullNext . (ix i2) .~ i0 &
        hullPrev . (ix i1) .~ i0 & 
        hullTri . (ix i0) .~ 0 & 
        hullTri . (ix i1) .~ 1 & 
        hullTri . (ix i2) .~ 2 & 
        hashEdge (points!i0) i0 &
        hashEdge (points!i1) i1 &
        hashEdge (points!i2) i2 


-- point "methods"

orient :: V2 Float ->  V2 Float -> V2 Float -> Bool
orient a q r =  ((q^._y - a^._y) * (r^._x - q^._x)) - ((q^._x - a^._x) * (r^._y - q^._y)) < 0
    
circumdelta :: V2 Float -> V2 Float -> V2 Float -> V2 Float
circumdelta a b c = V2 x y
  where
    d = b ^-^ a
    e = c ^-^ a
    bl = qd a b
    cl = qd a c
    dl = 0.5 / ((d^._x * e^._y) - (d^._y * e^._x))
    x = ((e^._y * bl) - (d^._y * cl)) * dl
    y = ((d^._x * cl) - (e^._x * bl)) * dl

circumradius2 :: V2 Float -> V2 Float -> V2 Float -> Float
circumradius2 a b c = quadrance (circumdelta a b c)

circumcenter :: V2 Float -> V2 Float -> V2 Float -> V2 Float
circumcenter a b c = a ^+^ (circumdelta a b c)

inCircle :: (V2 Float, V2 Float, V2 Float) -> V2 Float -> Bool
inCircle (a, b, c) p = term1 - term2 + term3 < 0
  where
    d = a ^-^ p
    e = b ^-^ p
    f = c ^-^ p
    ap = qdA a p
    bp = qdA b p
    cp = qdA c p 
    term1 = d^._x * (e^._y * cp - bp * f^._y)
    term2 = d^._y * (e^._x * cp - bp * f^._x)
    term3 = ap * (e^._x * f^._y - e^._y * f^._x)

nearlyEqual :: V2 Float -> V2 Float -> Bool
nearlyEqual a b = nearZero $ a ^-^ b

-- Triangle methods

nextHalfedge :: Int -> Int
nextHalfedge i | i `mod` 3 == 2 = i - 2
               | otherwise = i + 1

prevHalfedge :: Int -> Int
prevHalfedge i | i `mod` 3 == 0 = i + 2
               | otherwise = i - 1


boundingBox :: Vector (V2 Float) -> (V2 Float, V2 Float)
boundingBox ps = (
    V2 (minimum (ps^..traverse._x)) (minimum (ps^..traverse._y)),
    V2 (maximum (ps^..traverse._x)) (maximum (ps^..traverse._y))
    )

midpoint :: Vector (V2 Float) -> V2 Float
midpoint ps = let (lo, hi) = boundingBox ps in
              0.5 *^ (lo ^+^ hi)

qdANonSame :: V2 Float -> V2 Float -> Float
qdANonSame a b | nearlyEqual a b = 1/0
               | otherwise = qdA a b

closestPoint :: V2 Float  -> Vector (V2 Float) -> Int
closestPoint a ps = Vec.minIndexBy (compare `on` (qdA a)) ps

closestNonEqualPoint :: V2 Float  -> Vector (V2 Float) -> Int
closestNonEqualPoint a ps = Vec.minIndexBy (compare `on` (qdANonSame a)) ps

smallestCircumradius :: V2 Float -> V2 Float -> Vector (V2 Float) -> Int
smallestCircumradius  a b ps = i
  where 
    notAOrB = Vec.filter (/= a) $ Vec.filter (/=b) ps
    smallest = Vec.minimumBy (compare `on` (circumradius2 a b)) $ notAOrB
    Just i = Vec.elemIndex smallest ps

findSeedTriangle :: Vector (V2 Float) -> (Int, Int, Int)
findSeedTriangle ps = if (orient (ps!a) (ps!b) (ps!c)) then (a, c, b) else (a, b, c)
  where
    mid = midpoint ps
    a = closestPoint mid ps
    b = closestNonEqualPoint (ps!a) ps
    c = smallestCircumradius (ps!a) (ps!b) ps

-- hull methods
--
findVisibleEdge :: V2 Float -> Vector (V2 Float) -> Hull -> (Int, Bool)
findVisibleEdge p points h = findRecursive start
  where
    key = hashKey h p 
    len = Vec.length $ h^.hullHash
    cond j = case h ^? hullHash.(ix ((key + j) `mod` len)) of
                Nothing -> False
                Just s | s == empty -> False 
                       | otherwise -> (h ^. hullNext)!s /=  empty
    start'= case filter cond [0..len] of
                (j:_) -> (h^.hullHash) ! ((key + j) `mod` len)
                [] -> (h^. hullHash) ! (key `mod` len)
    start = (h^.hullPrev) ! start'
    findRecursive e = if (orient p (points ! e) (points ! ((h ^. hullNext) !e)))
                        then (e, e == start)
                        else (if ((h^.hullNext)!e == start) 
                                then (empty, False) 
                                else findRecursive ((h ^. hullNext) ! e) 
                             )

-- triangulation methods

legalizeOtherSideHalfedge :: Int -> Int -> Int -> Hull -> Hull
legalizeOtherSideHalfedge a bl e h = h'
  where
    hte = (h^.hullTri) ! e
    e' = (h ^.hullNext) ! e
    h' | hte == bl = h & hullTri . (ix e) .~ a
       | otherwise = if e' == h^.start then h else legalizeOtherSideHalfedge a bl e' h

legalize :: Vector (V2 Float) -> Int -> State (Hull, Delaunay) Int
legalize points a = do
    b <- ((!a). (^._2.halfedges)) <$> get
    let ar = prevHalfedge a
    if b == empty 
        then return ar
        else do
            let al = nextHalfedge a
            let bl = prevHalfedge b
            p0 <- ((!ar).(^._2.triangles)) <$> get
            pr <- ((!a) .(^._2.triangles)) <$> get
            pl <- ((!al).(^._2.triangles)) <$> get
            pone <- ((!bl).(^._2.triangles)) <$> get
            let illegal = inCircle (points ! p0, points ! pr, points ! pl) (points ! pone)
            if (not illegal) 
                then return ar
                else do
                    hbl <- ((! bl).(^._2.halfedges)) <$> get
                    har <- ((! ar).(^._2.halfedges)) <$> get
                    strt <- (^._1.start) <$> get
                    if hbl == empty 
                        then modify (_1 %~ (legalizeOtherSideHalfedge a bl strt))
                        else return()
                    zoom _2 $ do 
                        modify $ triangles . (ix a) .~ pone 
                        modify $ triangles . (ix b) .~ p0 
                        modify $ halfedges . (ix a) .~ hbl
                        modify $ halfedges . (ix b) .~ har
                        modify $ halfedges . (ix ar) .~ bl 
                        modify (if hbl /= empty then halfedges . (ix hbl) .~ a else id )
                        modify (if har /= empty then halfedges . (ix har) .~ b else id )
                        modify (if bl /= empty then halfedges . (ix bl) .~ ar else id ) 
                    let br = nextHalfedge b
                    void $  legalize points a
                    legalize points br 
     
walkForward :: Vector (V2 Float) -> Int -> Int -> State (Hull, Delaunay) Int
walkForward points i n = do
    let p = points ! i
    q <- ((!n). (^._1.hullNext)) <$> get
    let stillWalking = orient p (points ! n) (points ! q)
    if stillWalking 
        then do
            hti <- ((!i).(^._1.hullTri)) <$> get
            htn <- ((!n).(^._1.hullTri)) <$> get
            tp <- addTriangle (n, i, q) (hti, empty, htn)
            tq <- legalize points (tp+2) 
            modify $ _1 . hullTri . (ix i) .~ tq
            modify $ _1 . hullNext . (ix n) .~ empty
            walkForward points i q
        else
            return n

walkBack ::Vector (V2 Float) -> Int -> Int -> State (Hull, Delaunay) Int
walkBack points i e = do
    let p = points ! i
    q <- ((!e).(^._1.hullPrev)) <$> get
    let stillWalking =  orient p (points ! q) (points !e)
    if stillWalking
        then do
            hte <- ((!e).(^._1.hullTri)) <$> get
            htq <- ((!q).(^._1.hullTri)) <$> get
            tp <- addTriangle (q, i, e) (empty, hte, htq)
            void $ legalize points (tp+2)
            modify $ _1 . hullTri . (ix q) .~ tp 
            modify $ _1 . hullNext . (ix e) .~ empty
            walkBack points i q
        else return e 

triangulationStep :: Vector (V2 Float) -> Int -> State (Hull, Delaunay) ()
triangulationStep points i = do 
    let p = points ! i
    (e, doWalkBack) <- ((findVisibleEdge p points). (^._1)) <$> get 
    if (e == empty) then return ()
                    else do
        hne <- ((!e).(^._1.hullNext)) <$> get
        hte <- ((!e).(^._1.hullTri)) <$> get
        tp <- addTriangle (e, i, hne) (empty, empty, hte)
        ti <- legalize points (tp+2)
        modify $ _1 . hullTri . (ix i) .~ ti
        modify $ _1 . hullTri . (ix e) .~ tp
        n <- ((!e) . (^._1.hullNext)) <$> get
        n' <- walkForward points i n
        e' <- if doWalkBack then walkBack points i e else return e
        zoom _1 $ do
            modify $ hullPrev . (ix i) .~ e'
            modify $ hullNext . (ix i) .~ n'
            modify $ hullPrev . (ix n') .~ i
            modify $ hullNext . (ix e') .~ i
            modify $ start .~ e'
            modify $ hashEdge p i 
            modify $ hashEdge (points ! e') e'

triangulate :: [V2 Float] -> Delaunay
triangulate ps = (runState triangulate' (hull, delaunay))^._2._2
  where 
    points = (Vec.fromList ps) 
    len = Vec.length points
    (a, b, c) = findSeedTriangle points
    center = circumcenter (points!a) (points!b) (points!c)
    outwards = filter (/= c) $ filter (/=b) $ filter (/=a) $ (fmap fst) $ sortBy (compare `on` ((qdA center).snd)) $ zip (enumFrom 0) ps
    delaunay = newDelaunay points
    hull = newHull len center a b c points
    triangulate' = do
                    void $ addTriangle (a, b, c) (empty, empty, empty)
                    forM_ outwards (triangulationStep points) 
 
-- A Voronoi diagram is represented as a graph, where the nodes are the points of the diagram
-- and the edges are the endpoints of the side of the cells common to adjacent points
type Voronoi = GR.AdjacencyMap (First (V2 Float, V2 Float)) (V2 Float)

triElem :: Eq a => a -> (a, a, a) -> Bool
triElem x = or . (fmap (==x)) . (^..each) 

angle' :: V2 Float -> V2 Float -> Float
angle' a b = let V2 x y = b ^-^ a in atan2 x y

toEdge :: Vector (V2 Float) -> Vector (V2 Float) -> Delaunay -> Int -> Maybe Voronoi
toEdge points circumcenters d e = do
    i1 <- (d^.triangles)!?e
    p1 <- points !? i1
    he <- (d^.halfedges) !? e
    i2 <- (d^.triangles) !? he
    p2 <- points !? i2
    c1 <- circumcenters !? (e `div` 3)
    c2 <- circumcenters !? (he `div` 3)
    return (p1 -< (First . Just $  (c1, c2)) >- p2)
    
makeVoronoi :: Delaunay -> Voronoi
makeVoronoi d = GR.overlays edges
  where 
    ps = d ^.points
    tris = Vec.fromList . trios . Vec.toList $  d^.triangles
    circumcenters = (\(a, b, c) -> circumcenter (ps!a) (ps!b) (ps!c)) <$> tris
    edges = catMaybes $ (toEdge ps circumcenters d) <$> [0 .. (Vec.length (d^.triangles))] 

toEdges' :: [a] -> [(a, a)]
toEdges' (x1:x2:xs) = (x1, x2):(toEdges' (x2:xs))
toEdges' _ = []

toEdges :: [a] -> [(a, a)]
toEdges xs = (last xs, head xs):(toEdges' xs) 

voronoiHalfedges :: Voronoi -> [(V2 Float, V2 Float)]
voronoiHalfedges v = (getFirst <$> ((GR.edgeList v)^..traverse._1))^..traverse.traverse
    
