module Svg
    ( pointsAsSvg, 
      linesAsSvg, 
      pointsAndLinesAsSvg
    ) where

import Linear.V2 
import Linear.Vector
import Control.Lens
import Poisson (poissonSample)
import Control.Monad.Random.Lazy

svgDoctype :: String
svgDoctype = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" ++
                "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n" ++
                "  \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"

toSvg :: V2 Float -> String -> String
toSvg (V2 w h) content = svgDoctype ++ "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"" ++
    (show w) ++ "\" height=\"" ++
    (show h) ++ "\">\n" ++
    content ++
    "</svg>"


linesAsSvg :: V2 Float -> [(V2 Float, V2 Float)] -> String
linesAsSvg size lines = toSvg size $ linesAsSvgSnippet lines

linesAsSvgSnippet :: [(V2 Float, V2 Float)] -> String
linesAsSvgSnippet lines = 
                  concat $ (\l -> "    <line " ++
                       "x1=\"" ++ (show $ l^._1._x) ++ 
                    "\" y1=\"" ++ (show $ l^._1._y) ++ 
                    "\" x2=\"" ++ (show $ l^._2._x) ++ 
                    "\" y2=\"" ++ (show $ l^._2._y) ++ 
                    "\" stroke=\"blue\" />\n" 
                ) <$> lines

pointsAsSvg :: V2 Float -> Float -> [V2 Float] -> String
pointsAsSvg size r points = toSvg size $ pointsAsSvgSnippet r points

pointsAsSvgSnippet ::Float -> [V2 Float] -> String
pointsAsSvgSnippet r p = 
                  concat $ (\v -> "    <circle cx=\"" ++ (show $ v^._x) ++ 
                    "\" cy=\"" ++ (show $ v^._y) ++ 
                    "\" r=\"" ++ (show r) ++
                    "\" stroke=\"red\" fill=\"white\" fill-opacity=\"0.1\"/>\n" 
                ) <$> p

pointsAndLinesAsSvg :: V2 Float -> Float -> [V2 Float] -> [(V2 Float, V2 Float)] -> String
pointsAndLinesAsSvg size r points lines = toSvg size $ (pointsAsSvgSnippet r points) ++ (linesAsSvgSnippet lines)
