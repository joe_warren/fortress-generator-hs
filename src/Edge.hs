module Edge
( extractEdges
) where

import Delaunay (Voronoi (..))
import qualified Algebra.Graph.Labelled.AdjacencyMap as GR
import Algebra.Graph.Labelled.AdjacencyMap ((-<), (>-))
import Data.Monoid (First (..), getFirst)
import Linear.V2 
import Linear.Vector hiding (Vector)
import Control.Lens

first :: Iso' (First a) (Maybe a)
first = iso getFirst First

extractEdges :: ((V2 Float) -> Bool) -> Voronoi -> [(V2 Float, V2 Float)]
extractEdges f v = (filter (\(e, a, b) -> f a /= f b) (GR.edgeList v)) ^.. traverse . _1 . first . traverse

